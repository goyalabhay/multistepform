const MarineBlue = "hsl(213, 96%, 18%)";
const PurplishBlue = "hsl(243, 100%, 62%)";
const PastelBlue = "hsl(228, 100%, 84%)";
const LightBlue = "hsl(206, 94%, 87%)";
const StrawberryRed = "hsl(354, 84%, 57%)";
const CoolGray = "hsl(231, 11%, 63%)";
const LightGray = "hsl(229, 24%, 87%)";
const Magnolia = "hsl(217, 100%, 97%)";
const Alabaster = "hsl(231, 100%, 99%)";
const White = "hsl(0, 0%, 100%)";

let currentPage = 0;
let yearToggle = false;
let addOnClicked = false;
let radioName;
let yearOrMonth = "(Month";
let yearOrMonthPrice;
let totalYearOrMonth;
let totalAmountYearOrMonth;
let finalamount;

const number = document.querySelectorAll(".number");
const pages = document.querySelectorAll(".page");
const nextButton = document.querySelector(".button");
const allBoxes = document.querySelectorAll(".box");
const goBack = document.querySelector(".goBack");
const buttons = document.querySelector(".buttons");
const field = document.querySelectorAll(".field");
const monthly = document.querySelector(".monthly");
const yearly = document.querySelector(".yearly");
const toggle = document.querySelector("#toggle");
const yearRate = document.querySelectorAll(".year-rate");
const monthRate = document.querySelectorAll(".rate");
const allCheckboxes = document.querySelectorAll(".checkbox");
const prices = document.querySelectorAll(".price");
const billType = document.querySelector(".bill-type");
const mainBill = document.querySelector(".main-bill-amount");
const extraBills = document.querySelectorAll(".extra-bill-amount");
const total = document.querySelector(".total");
const totalAmount = document.querySelector(".total-amount");
const service = document.querySelector(".extra-bill-1");
const storage = document.querySelector(".extra-bill-2");
const profile = document.querySelector(".extra-bill-3");
const change = document.querySelector(".change");

const addOnsObject = {
  0: {
    isSelected: false,
    monthly: 1,
    yearly: 10,
  },
  1: {
    isSelected: false,
    monthly: 2,
    yearly: 20,
  },
  2: {
    isSelected: false,
    monthly: 2,
    yearly: 20,
  },
};

let addOnPrice = 0;

allCheckboxes.forEach((checkbox, index) => {
  checkbox.addEventListener("click", function () {
    checkbox.parentNode.parentNode.parentNode.classList.toggle("clicked");
    if (addOnsObject[`${index}`].isSelected) {
      addOnsObject[`${index}`].isSelected = false;
    } else {
      addOnsObject[`${index}`].isSelected = true;
    }
  });
});

number[currentPage].classList.add("coloring");

goBack.style.display = "none";

monthly.classList.add("marineblue");

let firstPageCheck = () => {
  let correctBoxes = 0;
  allBoxes.forEach((box, index) => {
    if (box.value.length !== 0) {
      correctBoxes++;
    } else {
      field[index].style.display = "flex";
      box.style.borderColor = StrawberryRed;
    }
  });
  if (correctBoxes === 3) {
    return true;
  } else {
    return false;
  }
};

let numberColoringNextPage = (currentPage) => {
  if (currentPage < 3) {
    number[currentPage + 1].classList.toggle("coloring");
    number[currentPage].classList.toggle("coloring");
  }
};

nextButton.addEventListener("click", nextPage);

function nextPage() {
  if (currentPage === 0) {
    if (firstPageCheck()) {
      numberColoringNextPage(currentPage);
      pages[currentPage].style.display = "none";
      pages[currentPage + 1].style.display = "flex";
      goBack.style.display = "flex";
      currentPage++;
    }
  } else if (currentPage === 1) {
    const radio = document.querySelector("input[type=radio]:checked");
    radioName = radio.id;
    yearOrMonthPrice =
      radio.nextElementSibling.querySelector(".rate").innerText;
    if (radio) {
      numberColoringNextPage(currentPage);
      goBack.style.display = "flex";
      pages[currentPage].style.display = "none";
      pages[currentPage + 1].style.display = "flex";
      currentPage++;
    }
  } else if (currentPage === 2) {
    billType.innerText = `${radioName} ${yearOrMonth}ly)`;
    mainBill.innerText = `${yearOrMonthPrice}`;
    numberColoringNextPage(currentPage);
    goBack.style.display = "flex";
    pages[currentPage].style.display = "none";
    pages[currentPage + 1].style.display = "flex";
    nextButton.innerText = "Confirm";
    currentPage++;
    if (addOnsObject["0"].isSelected) {
      service.style.display = "flex";
      if (yearToggle) {
        addOnPrice += addOnsObject["0"].yearly;
      } else {
        addOnPrice += addOnsObject["0"].monthly;
      }
    }
    if (addOnsObject["1"].isSelected) {
      storage.style.display = "flex";
      if (yearToggle) {
        addOnPrice += addOnsObject["1"].yearly;
      } else {
        addOnPrice += addOnsObject["1"].monthly;
      }
    }
    if (addOnsObject["2"].isSelected) {
      profile.style.display = "flex";
      if (yearToggle) {
        addOnPrice += addOnsObject["2"].yearly;
      } else {
        addOnPrice += addOnsObject["2"].monthly;
      }
    }
    let planPrice = 0;
    if (radioName === "Arcade") planPrice = 9;
    else if (radioName === "Advanced") planPrice = 12;
    else if (radioName === "Pro") planPrice = 15;
    if (yearToggle) planPrice *= 10;
    finalamount = addOnPrice + planPrice;
    if (yearToggle) totalAmount.innerText = `$${finalamount}/yr`;
    else totalAmount.innerText = `$${finalamount}/mo`;
  } else if (currentPage === 3) {
    numberColoringNextPage(currentPage);

    goBack.style.display = "flex";
    pages[currentPage].style.display = "none";
    pages[currentPage + 1].style.display = "flex";
    buttons.style.display = "none";
    currentPage++;
  }
}

let numberColoringPreviousPage = (previousPage) => {
  if (currentPage <= 3) {
    number[currentPage].classList.toggle("coloring");
    number[currentPage - 1].classList.toggle("coloring");
  }
};

goBack.addEventListener("click", previousPage);

function previousPage() {
  numberColoringPreviousPage(currentPage);

  if (currentPage === 1) {
    pages[currentPage - 1].style.display = "flex";
    pages[currentPage].style.display = "none";
    goBack.style.display = "none";
  } else if (currentPage === 2) {
    pages[currentPage - 1].style.display = "flex";
    pages[currentPage].style.display = "none";
  } else if (currentPage === 3) {
    resetAddOns();
    pages[currentPage - 1].style.display = "flex";
    pages[currentPage].style.display = "none";
    nextButton.innerText = "Next Step";
  }
  currentPage--;
}

toggle.addEventListener("click", function () {
  if (!yearToggle) {
    extraBills.forEach((extraBill) => {
      extraBill.innerText = extraBill.innerText.replace("/mo", "0/yr");
    });
    yearOrMonth = "(Year";
    total.innerText = "Total (Per Year)";
    totalAmount.innerText = totalAmount.innerText.replace("/mo", "0/yr");
    yearToggle = true;

    yearRate.forEach((yRate, index) => {
      yRate.style.display = "flex";
      monthRate[index].innerText = monthRate[index].innerText.replace(
        "/mo",
        "0/yr"
      );
    });
    prices.forEach((price) => {
      price.innerText = price.innerText.replace("/mo", "0/yr");
    });
  } else {
    extraBills.forEach((extraBill) => {
      extraBill.innerText = extraBill.innerText.replace("0/yr", "/mo");
    });
    yearOrMonth = "(Month";
    total.innerText = "Total (Per Month)";
    totalAmount.innerText = totalAmount.innerText.replace("0/yr", "/mo");
    yearToggle = false;
    yearRate.forEach((yRate, index) => {
      yRate.style.display = "none";
      monthRate[index].innerText = monthRate[index].innerText.replace(
        "0/yr",
        "/mo"
      );
    });
    prices.forEach((price) => {
      price.innerText = price.innerText.replace("0/yr", "/mo");
    });
  }
  yearly.classList.toggle("marineblue");
  monthly.classList.toggle("marineblue");
});

function resetAddOns() {
  addOnPrice = 0;
  allCheckboxes.forEach((checkbox) => {
    checkbox.parentNode.parentNode.parentNode.classList.remove("clicked");
    checkbox.checked = false;
  });
  addOnsObject["0"].isSelected = false;
  addOnsObject["1"].isSelected = false;
  addOnsObject["2"].isSelected = false;
  service.style.display = "none";
  storage.style.display = "none";
  profile.style.display = "none";
}

change.addEventListener("click", function () {
  resetAddOns();
  currentPage = 1;
  pages[3].style.display = "none";
  pages[1].style.display = "flex";
  number[1].classList.add("coloring");
  number[3].classList.remove("coloring");
  nextButton.innerText = "Next Step";
});
